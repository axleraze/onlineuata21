using System;
namespace ConditionStatements
{

    public static class Program
    {

             //public static void Main()
             public static int Task1(int n)
             {
                 //   int n = int.Parse(Console.ReadLine());
                if (n <= 0)
                    {
                        int b = Math.Abs(n);
                        //   Console.WriteLine(b);
                        return (b);
                    }
                    else
                    {
                        int b = n * n;
                        //    Console.WriteLine(b);
                        return (b);
                    }
                  //  Console.ReadKey();

              }
    
           //public static void Main()
              public static int Task2(int d)
              {
              // int d = int.Parse(Console.ReadLine());
              int z = 0;
            
                int da = d / 100;
                int db = (d / 10) % 10;
                int dc = d % 10;

                int max = Math.Max(Math.Max(da, db), dc);
                int min = Math.Min(Math.Min(da, db), dc);
                int mdl = da + db + dc - max - min;

                z = max * 100 + mdl * 10 + min;
                // Console.WriteLine(z);
                return (z);

              }
    }
}
